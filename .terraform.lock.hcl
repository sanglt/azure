# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/gitlabhq/gitlab" {
  version = "3.5.0"
  hashes = [
    "h1:OPAh3smkOF5k4fkr4pDrKFBfFgx+oGq96Zvn+HEpNRs=",
    "zh:20a7e10d88020a88f1aedd93d1de9a70efcc192f5c630df6a7818c4c3e501ac5",
    "zh:30c2f168869e7b243c844610bba5964de4acec9d0bb1255fc1ee6b97645dc398",
    "zh:4429f353f907f496997590113f1552db153d1b5793a64b06dd30ddd69e4c5c9b",
    "zh:9ab8c83053e8af0722e2aa04bc676a40543cc12f82e250d711bfaf2a791b58b1",
    "zh:a51dacd1f2022ac3edada0541327b87a7886ad981c32d29fd0bb48139a0b1360",
    "zh:ab568a4a19e272f2ead4d6e4f2a8ae61a18a952c92de741dfa6b1f501a18d30c",
    "zh:c7a7440c9b4698524506a6cbf8c8daf0dae32af3e245882b1ae2217c5a53bb9d",
    "zh:cc344adba161de020c677f7a6e233d829ffdfcd80ca154633caa5b5f23735346",
    "zh:ce6f02db7da21c308e824ea3d051e182f310fdba74fd4d40fedceb539b97daed",
    "zh:e44c8cde080e8690533623d4c56d9070cb4c720d12774e341c5b75c20291cd2d",
    "zh:ebdc5b7c23f3a0be2776402782312277cbd5ec5b54b8302eb64934e96e6c9a28",
  ]
}

provider "registry.terraform.io/hashicorp/azurerm" {
  version = "2.49.0"
  hashes = [
    "h1:ia3Q3jQW/tSNABfSm+ByOzWxSA290KgzH3oVP5hVEKc=",
    "zh:02c5ea5727d23c33bbba4ae6b58ea90ed785a4597d989c9940ed1e215a59c3c8",
    "zh:4c22d5422a5475a611b69337203e91531dab009fc2be14ec4ca3585eb581dcc0",
    "zh:7a59a0231feabc4285a411272e67e66abcce953a5065ffa005ac73418c5aa367",
    "zh:7ea4b26c706cb56f80fb4c177ac6197b65f7784ef8a0ba8e7a821c9b2b509af2",
    "zh:85028b955dccbb7a3e73dfde3f59edf05e3fc694d9298c46bd86ddc1045e0e83",
    "zh:8ac48666a08169569d2f87288bc46e4a936ff182d303009ebcabdcefb0e81167",
    "zh:92fcb8399f1818685891748a0e9ae004488de27f68a06b3747bc30d20e3d780d",
    "zh:9d4db323f9e8d7861d928171bef18b0e3bb0c41c8222f2eb81cad39a2901fe3b",
    "zh:a30c943ca88197fa0ee0f4fbd5a3677751636a3e1045046df5b1d64f560545e9",
    "zh:d4b410bc00e764abcf35133ba8beae053fa2d9dd4fa2745828ea88113ab9af83",
    "zh:e8d18c8abe7b131dcc697036debea1303993f870fb7a5a36d4620c2add468981",
  ]
}

provider "registry.terraform.io/hashicorp/kubernetes" {
  version = "2.0.2"
  hashes = [
    "h1:vNrgTrqsLcL2Uw8kr89ZIq2NF858MZ15sLtNfd55hVA=",
    "zh:4e66d509c828b0a2e599a567ad470bf85ebada62788aead87a8fb621301dec55",
    "zh:55ca6466a82f60d2c9798d171edafacc9ea4991aa7aa32ed5d82d6831cf44542",
    "zh:65741e6910c8b1322d9aef5dda4d98d1e6409aebc5514b518f46019cd06e1b47",
    "zh:79456ca037c19983977285703f19f4b04f7eadcf8eb6af21f5ea615026271578",
    "zh:7c39ced4dc44181296721715005e390021770077012c206ab4c209fb704b34d0",
    "zh:86856c82a6444c19b3e3005e91408ac68eb010c9218c4c4119fc59300b107026",
    "zh:999865090c72fa9b85c45e76b20839da51714ae429d1ab14b7d8ce66c2655abf",
    "zh:a3ea0ae37c61b4bfe81f7a395fb7b5ba61564e7d716d7a191372c3c983271d13",
    "zh:d9061861822933ebb2765fa691aeed2930ee495bfb6f72a5bdd88f43ccd9e038",
    "zh:e04adbe0d5597d1fdd4f418be19c9df171f1d709009f63b8ce1239b71b4fa45a",
  ]
}

provider "registry.terraform.io/terraform-providers/gitlab" {
  version     = "3.5.0"
  constraints = ">= 2.9.0"
  hashes = [
    "h1:OPAh3smkOF5k4fkr4pDrKFBfFgx+oGq96Zvn+HEpNRs=",
    "zh:20a7e10d88020a88f1aedd93d1de9a70efcc192f5c630df6a7818c4c3e501ac5",
    "zh:30c2f168869e7b243c844610bba5964de4acec9d0bb1255fc1ee6b97645dc398",
    "zh:4429f353f907f496997590113f1552db153d1b5793a64b06dd30ddd69e4c5c9b",
    "zh:9ab8c83053e8af0722e2aa04bc676a40543cc12f82e250d711bfaf2a791b58b1",
    "zh:a51dacd1f2022ac3edada0541327b87a7886ad981c32d29fd0bb48139a0b1360",
    "zh:ab568a4a19e272f2ead4d6e4f2a8ae61a18a952c92de741dfa6b1f501a18d30c",
    "zh:c7a7440c9b4698524506a6cbf8c8daf0dae32af3e245882b1ae2217c5a53bb9d",
    "zh:cc344adba161de020c677f7a6e233d829ffdfcd80ca154633caa5b5f23735346",
    "zh:ce6f02db7da21c308e824ea3d051e182f310fdba74fd4d40fedceb539b97daed",
    "zh:e44c8cde080e8690533623d4c56d9070cb4c720d12774e341c5b75c20291cd2d",
    "zh:ebdc5b7c23f3a0be2776402782312277cbd5ec5b54b8302eb64934e96e6c9a28",
  ]
}
